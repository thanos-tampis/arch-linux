#!/bin/bash

# simple terminal info

# fallback
FB="unknown"

# colours
RESET=$'\e[0m'
for ((i=0; i<8; i++)); do
    printf -v FG${i} "%b" "\e[3${i}m"
    printf -v BG${i} "%b" "\e[4${i}m"
done

# distro name
if [[ -e /etc/os-release ]] && . /etc/os-release 2>/dev/null; then
    DIST="${ID,,}"
elif [[ -e /etc/lsb-release ]] && . /etc/lsb-release 2>/dev/null; then
    DIST="${DISTRIB_ID,,}"
fi

# system uptime
UPT="$(uptime -p)"
UPT="${UPT/up /}"
UPT="${UPT/ day?/d}"
UPT="${UPT/ hour?/h}"
UPT="${UPT/ minute?/m}"

# install date
if [[ -e /var/log/pacman.log ]]; then
    INST="$(sed 1q /var/log/pacman.log)"
    INST="${INST/ */}"
    INST="${INST/T*/}" # newer pacman adds a different timestamp (TXX:XX:XX+0000), we just want the date
    INST="${INST/\[/}"
    INST="${INST//\-/ }"
fi

# check if root user
USR="${FG4}${USER}"
(( UID == 0 )) &&USR="${FG1}root"

# login shell
SHLL="${SHELL##*/}"

# terminal name
TRM="${TERM/-256color/}"        # many terminals use a -256color suffix, remove it
TRM="${TRM/-unicode/}"          # rxvt can be rxvt-unicode, remove the -unicode suffix
TRM="${TRM/*-termite*/termite}" # termite is xterm-termite, truncate to just termite

# hostname
HOST="${HOST:-$(cat /etc/hostname 2>/dev/null)}"
HOST="${HOST:-$FB}"

# kernel version
KERN="${KERN:-$(uname -sr)}"
KERN="${KERN/-*/}"
KERN="${KERN,,}"

# MEM = used / total
FREE="$(free --mega)"
MB=$(awk 'NR==2 {print $3}' <<< "$FREE")
GB=$(awk 'NR==2 {printf "%.2f", $3 / 1000}' <<< "$FREE")
TOT=$(awk 'NR==2 {printf "%.2f", $2 / 1000}' <<< "$FREE")
if (( MB > 1000 )); then #  show in GB
    MEM="${GB:0:5}gb / ${TOT/\.*}gb"
else # show in MB
    MEM="${MB}mb / ${TOT/\.*}gb"
fi

# CPU threads and cores
THREADS=$(grep -c '^processor' /proc/cpuinfo)
THREADS=${THREADS:-1}
CORES=$(grep '^core' /proc/cpuinfo | sort | uniq | wc -l)
CORES=${CORES:-1}
# CPU usage needs to be divided by number of threads to get an average percentage
USAGE=$(ps aux | awk -v i="$THREADS" 'BEGIN{sum = 0} {sum += $3} END{printf "%.2f", sum / i}')
CPU="${CORES}c/${THREADS}t @ $USAGE% avg"

rep_char()
{
    local str char num

    char="${1:-=}"
    num="${2:-$((${#USR} + 3 + ${#HOST}))}"
    str="$(printf "%${num}s")"
    echo "${str// /$char}"
}

pkg_count()
{
    pacman -Qq 2>/dev/null | wc -l || echo "$FB"
}

cur_wm()
{
    if [[ -z $WM && $DISPLAY ]] && hash xprop >/dev/null 2>&1; then
        ID="$(xprop -root -notype _NET_SUPPORTING_WM_CHECK)"
        WM="$(xprop -id "${ID##* }" -notype -len 100 -f _NET_WM_NAME 8t)"
        WM="${WM/*WM_NAME = }"
        WM="${WM/\"}"
        WM="${WM/\"*}"
        WM="${WM,,}"
        echo "${WM:-$FB}"
    else
        echo "${WM:-tty$XDG_VTNR}"
    fi
}

clear
cat <<EOF

 ${FG5}--->$RESET ${USR}${FG2} @ ${FG6}${HOST:-$FB}
 ${FG5}$(rep_char '=')>
 ${FG6}->$RESET  OS$FG1:         ${FG4}${DIST:-$FB} 
 ${FG6}->$RESET  WM$FG1:         ${FG4}$(cur_wm)
 ${FG6}->$RESET  Term$FG1:       ${FG4}${TRM:-$FB}
 ${FG6}->$RESET  Shell$FG1:      ${FG4}${SHLL:-$FB}
 ${FG6}->$RESET  Packages$FG1:   ${FG4}$(pkg_count)
 ${FG6}->$RESET  Kernel$FG1:     ${FG4}${KERN:-$FB}
 ${FG6}->$RESET  Installed$FG1:  ${FG4}${INST:-$FB}
 ${FG6}->$RESET  Uptime$FG1:     ${FG4}${UPT:-$FB}
 
 ${FG6}->$RESET  Memory$FG1:     ${FG4}${MEM:-$FB}
 ${FG6}->$RESET  CPU$FG1:        ${FG4}${CPU:-$FB}

 $BG1    $BG2    $BG3    $BG4    $BG5    $BG6    $BG7    $RESET

EOF