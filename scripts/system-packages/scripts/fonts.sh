#!/bin/bash
set -e

echo "##############    Install various fonts   ##############"

# base font packages
fontBasePackages='
				ttf-opensans
				ttf-dejavu
				ttf-liberation
				noto-fonts
				ttf-font-awesome				
				'

for package in $fontBasePackages
do 
	
	sudo pacman -S $package --needed --noconfirm
	
done


# aur font packages
fontAurPackages='	
				ttf-weather-icons
				awesome-terminal-fonts		
				'

for package in $fontAurPackages
do 
	
	#checking if application is already installed or else install with aur helpers
	if pacman -Qi $package &> /dev/null; then

		echo "################################################################"
		echo "################## "$package" already installed"
		echo "################################################################"

	else

		#checking which helper is installed
		if pacman -Qi aurman &> /dev/null; then

			echo "Installing with aurman"
			aurman -S --noconfirm --noedit  $package

		elif pacman -Qi trizen &> /dev/null; then
			
			echo "Installing with trizen"
			trizen -S --noconfirm --noedit  $package
			 	
		elif pacman -Qi yay &> /dev/null; then

			echo "Installing with yay"
			yay -S --noconfirm $package
				  	
		fi

		# Just checking if installation was successful
		if pacman -Qi $package &> /dev/null; then
		
		echo "################################################################"
		echo "#########  "$package" has installed successfully"
		echo "################################################################"

		else

		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo "!!!!!!!!!  "$package" has NOT been installed"
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

		fi

	fi
		
done


echo "################################################################"
echo "##############    Fonts installed successfully   ##############"
echo "################################################################"
