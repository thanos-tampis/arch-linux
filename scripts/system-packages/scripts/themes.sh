#!/bin/bash
set -e

echo "################################################################"
echo "#########    Install various themes and icon-themes   ##########"
echo "################################################################"

# These removed as they take so much time: arc-icon-theme-git, paper-icon-theme-git

# aur theme packages
themeAurPackages='
				openbox-themes				
				arc-gtk-theme
				numix-gtk-theme-git		
				gtk-theme-numix-solarized			
				yaru-gtk-theme		
				yaru-icon-theme
				papirus-icon-theme
				'

for package in $themeAurPackages
do 
	
	#checking if application is already installed or else install with aur helpers
	if pacman -Qi $package &> /dev/null; then

		echo "################################################################"
		echo "################## "$package" already installed"
		echo "################################################################"

	else

		#checking which helper is installed
		if pacman -Qi aurman &> /dev/null; then

			echo "Installing with aurman"
			aurman -S --noconfirm --noedit  $package

		elif pacman -Qi trizen &> /dev/null; then
			
			echo "Installing with trizen"
			trizen -S --noconfirm --noedit  $package
			 	
		elif pacman -Qi yay &> /dev/null; then

			echo "Installing with yay"
			yay -S --noconfirm $package
				  	
		fi

		# Just checking if installation was successful
		if pacman -Qi $package &> /dev/null; then
		
		echo "################################################################"
		echo "#########  "$package" has installed successfully"
		echo "################################################################"

		else

		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo "!!!!!!!!!  "$package" has NOT been installed"
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

		fi

	fi
		
done


echo "################################################################"
echo "##############    Various themes installed successfully   ##############"
echo "################################################################"
