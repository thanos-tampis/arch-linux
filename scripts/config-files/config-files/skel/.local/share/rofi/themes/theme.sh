#!/usr/bin/env bash

#
## Rofi   : Launcher (Modi Drun, Run, File Browser, Window)
#

dir="$HOME/.local/share/rofi/themes/"
theme='style-1'

## Run
rofi \
    -show drun \
    -theme ${dir}/${theme}.rasi
