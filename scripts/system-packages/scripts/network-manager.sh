#!/bin/bash
set -e

echo "##############    Install and enable Network Manager stuff   ##############"

# base network packages
networkBasePackages='
				networkmanager
				networkmanager-openvpn 
				networkmanager-pptp 
				network-manager-applet
				'

for package in $networkBasePackages
do 
	
	sudo pacman -S $package --needed --noconfirm
	
done

# enable Network Manager service
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service

# aur network packages
networkAurPackages='	
				networkmanager-dmenu-git			
				'

for package in $networkAurPackages
do 
	
	#checking if application is already installed or else install with aur helpers
	if pacman -Qi $package &> /dev/null; then

		echo "################################################################"
		echo "##############    "$package" already installed   ##############"
		echo "################################################################"

	else

		#checking which helper is installed
		if pacman -Qi aurman &> /dev/null; then

			echo "Installing with aurman"
			aurman -S --noconfirm --noedit  $package

		elif pacman -Qi trizen &> /dev/null; then
			
			echo "Installing with trizen"
			trizen -S --noconfirm --noedit  $package
			 	
		elif pacman -Qi yay &> /dev/null; then

			echo "Installing with yay"
			yay -S --noconfirm $package
				  	
		fi

		# Just checking if installation was successful
		if pacman -Qi $package &> /dev/null; then
		
		echo "################################################################"
		echo "#########  "$package" has been installed successfully"
		echo "################################################################"

		else

		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo "!!!!!!!!!  "$package" has NOT been installed"
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

		fi

	fi
	
done


echo "################################################################"
echo "######    Network Manager stuff installed successfully   #######"
echo "################################################################"
