#!/bin/bash
set -e

echo "##############    Install and enable Bluetooth   ##############"

# base Bluetooth packages
bluetoothBasePackages='
				bluez
				bluez-utils
				bluez-tools
				bluez-plugins
				blueman
				'

for package in $bluetoothBasePackages
do 
	
	sudo pacman -S $package --needed --noconfirm
	
done

# enable Bluetooth Manager service
sudo systemctl enable bluetooth.service
sudo systemctl start bluetooth.service

# aur bluetooth codec packages
bluetoothAurPackages='	
				libldac			
				'

for package in $bluetoothAurPackages
do 
	
	#checking if application is already installed or else install with aur helpers
	if pacman -Qi $package &> /dev/null; then

		echo "################################################################"
		echo "##############    "$package" already installed   ##############"
		echo "################################################################"

	else

		#checking which helper is installed
		if pacman -Qi aurman &> /dev/null; then

			echo "Installing with aurman"
			aurman -S --noconfirm --noedit  $package

		elif pacman -Qi trizen &> /dev/null; then
			
			echo "Installing with trizen"
			trizen -S --noconfirm --noedit  $package
			 	
		elif pacman -Qi yay &> /dev/null; then

			echo "Installing with yay"
			yay -S --noconfirm $package
				  	
		fi

		# Just checking if installation was successful
		if pacman -Qi $package &> /dev/null; then
		
		echo "################################################################"
		echo "#########  "$package" has been installed successfully"
		echo "################################################################"

		else

		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo "!!!!!!!!!  "$package" has NOT been installed"
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

		fi

	fi
	
done


echo "################################################################"
echo "#######    Bluetooth and codecs installed successfully   #######"
echo "################################################################"
