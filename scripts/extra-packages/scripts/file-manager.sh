#!/bin/bash
set -e

echo "################################################################"
echo "#########   Install Thunar File Manager  ################"
echo "################################################################"

# base font packages
fileManagerBasePackages='
				thunar
				thunar-archive-plugin 
				thunar-media-tags-plugin
				thunar-volman
				tumbler 
				ffmpegthumbnailer
				catfish
				gvfs
				gvfs-nfs
				gvfs-mtp
				gvfs-afc
				gvfs-smb
				file-roller 
				fsarchiver 
				udiskie 
				udisks2
				'

for package in $fileManagerBasePackages
do 
	
	sudo pacman -S $package --needed --noconfirm
	
done


echo "################################################################"
echo "#########  Thunar File Manager Installed  ################"
echo "################################################################"
