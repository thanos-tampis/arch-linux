# Arch Openbox

Arch Openbox/DWM Auto-Rice Bootstrapping Scripts.

Script that auto installs and configuring a fully-functioning and minimal Openbox/DWM Arch Linux environment. They also depend heavy on Aur helpers for installing packages using either helper below:
- Yay (recommended)
- Trizen


## Running script: 

- You must have base Arch installed first (See root README file installation notes)   
- Once you have Arch running run 'sh setup.sh' and follow the on-screen instructions.
- **NOTE:** Please run the script as a user and NOT as root.

## After configuration:

- You may want to run *'obmenu-generator -p'* and edit files to replace Openbox default applications such as xterm and geany.

- Remove various Keyboard shortcuts as they conflict with the custom Openbox ones

- Set default applications from Openbox menu

- If Nvidia Graphics are installed, you may need to enable *'Force Full Composition Pipeline'* to avoid possible screen tearing.
	- If Full Composition is enabled please only keep this part under */etc/X11/xorg.conf.d/20-nvidia.conf* and remove default xorg.conf added by Nvidia as it may cause issues when using second monitor.
	- Also add a **Pacman hook** to avoid the possibility of forgetting to update initramfs after an NVIDIA driver upgrade.

- Although default Fonts are set under *.config/fontconfig/fonts.conf* it may worth checking under General Settings -> Appearance -> Fonts that 'Sans Regular' is the Default Font and 'Monospace Regular' is the Default Monospace Font.

- If tray icons are duplicate please check */etc/xdg/autostart*
	- System-wide desktop entries can be overridden by user-specific entries with the same filename under *~/.config/autostart*
	- To disable a system-wide entry, create an overriding entry containing Hidden=true.
	- Ref: **https://wiki.archlinux.org/index.php/XDG_Autostart**


## File/Directories explained

The script is divided into 4 section. You can start with section 1 and progress with the rest as each section is completed. In every script, we have 'set -e', so if any error occurs, the script will immediately stop running.


### Initial Openbox/DWM setup

Scripts included:

#### Update packman
Performs a system update

#### Mirrorlist reflector
Finds the fastest archlinux servers

#### Xorg
Installs Xorg, Graphic drivers etc

#### Lightdm
Installs Lightdm Login Manager

#### Openbox/DWM
Installs Core base packages


### System base packages

Scripts included:

#### Fonts
Getting Various fonts

#### Themes
Getting various Themes to help you customising

#### NetworkManager
Installs Networmanager, applets etc.

#### Sound
Installs various Sound utilities


### Extra packages

Scripts included:

#### FileManager
Thunar is the graphical File Manager, which gets installed alongside other various utilities

#### Extra Software
This is where we install Terminals, Web browsers, Image viewer/manipulation software, Graphics packages, IDEs and more.


### Config files setup

Scripts included:

#### Default configuration
Enabling pacman colors and copying Dunst files

#### Skel files
This includes a lot of configuration for Arch users. 

- We copy all these under /etc/skel/ because if a new Arch user is created then all these will be automatically copied under the new user's home directory.

- We also copy all these under the current user's home directory.

Including configuration for Openbox/DWM, Terminals, Networkmanager-dmenu, Menu(Polybar/Tint2) etc. 

#### Misc
Copying various images and Wallpapers 


peace 
