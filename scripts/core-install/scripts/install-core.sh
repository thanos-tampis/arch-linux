#!/bin/bash
set -e

echo "##############    Install Core Openbox stuff   ##############"

# base pacman packages
openboxBasePackages='
				openbox
				obconf								
				xfce4-settings
				xfce4-power-manager
				xfce4-notifyd
				xdg-user-dirs
				xdg-user-dirs-gtk
				gtk-engines
				gtk-engine-murrine
				gtk2
				gtk3
				gsimplecal
				libnotify
				dunst
				numlockx
				xdotool
				picom
				pacman-contrib
				brightnessctl
				'

for package in $openboxBasePackages
do 
	
	sudo pacman -S $package --needed --noconfirm
	
done


# base aur packages
openboxAurPackages='							 
				obmenu-generator
				libxrandr
				ksuperkey
				arandr
				nitrogen
				lxinput
				playerctl
				tint2				
				'

for package in $openboxAurPackages
do

	#checking if application is already installed or else install with aur helpers
	if pacman -Qi $package &> /dev/null; then

		echo "################################################################"
		echo "##############    "$package" already installed   ##############"
		echo "################################################################"

	else

		#checking which helper is installed
		if pacman -Qi aurman &> /dev/null; then

			echo "Installing with aurman"
			aurman -S --noconfirm --noedit  $package

		elif pacman -Qi trizen &> /dev/null; then
			
			echo "Installing with trizen"
			trizen -S --noconfirm --noedit  $package
			 	
		elif pacman -Qi yay &> /dev/null; then

			echo "Installing with yay"
			yay -S --noconfirm $package
				  	
		fi

		# Just checking if installation was successful
		if pacman -Qi $package &> /dev/null; then
		
		echo "################################################################"
		echo "#########  "$package" installed successfully"
		echo "################################################################"

		else

		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo "!!!!!!!!!  "$package" has NOT been installed"
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

		fi

	fi

done

