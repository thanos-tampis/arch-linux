#!/bin/bash
set -e

# Overall script with resources.
# Install software and configure each component

# Install various fonts
sh system-packages/scripts/fonts.sh

# Install various themes
sh system-packages/scripts/themes.sh

# Network Manager
sh system-packages/scripts/network-manager.sh

# Sound stuff
sh system-packages/scripts/sound.sh

# Bluetooth
sh system-packages/scripts/bluetooth.sh