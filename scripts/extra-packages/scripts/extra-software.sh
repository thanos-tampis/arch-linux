#!/bin/bash
set -e

# base extra software packages
extraSoftwareBasePackages='
				ufw
				desktop-file-utils
				polkit-gnome
				gnome-keyring
				gnome-screenshot
				sound-theme-freedesktop
				gpicview
				ranger
				scrot
				i3lock
				startup-notification
				volume_key
				ntfs-3g
				git
				p7zip
				unzip
				rofi
				htop
				less
				openssh
				mpv
				libmad
				conky
				wget
				dmidecode				
				curl
				rsync
				screen
				unrar
				tar
				net-tools				
				libreoffice-still
				evince
				zenity
				filezilla
				galculator
				neovim
				xclip
				jq
				rxvt-unicode
				urxvt-perls							
				imagemagick
				gimp
				'

for package in $extraSoftwareBasePackages
do 
	
	sudo pacman -S $package --needed --noconfirm
	
done

# aur extra software packages
extraSoftwareAurPackages='
				librewolf-bin
				google-chrome
				surfshark-client
				deadbeef
				gitkraken
				mintstick-git
				caffeine-ng
				clipit
				skippy-xd-git
				urxvt-resize-font-git	
				sublime-text-4
				visual-studio-code-bin
				'

for package in $extraSoftwareAurPackages
do 
	
	#checking if application is already installed or else install with aur helpers
	if pacman -Qi $package &> /dev/null; then

		echo "################################################################"
		echo "################## "$package" already installed"
		echo "################################################################"

	else

		#checking which helper is installed
		if pacman -Qi aurman &> /dev/null; then

			echo "Installing with aurman"
			aurman -S --noconfirm --noedit  $package

		elif pacman -Qi trizen &> /dev/null; then
			
			echo "Installing with trizen"
			trizen -S --noconfirm --noedit  $package
			 	
		elif pacman -Qi yay &> /dev/null; then

			echo "Installing with yay"
			yay -S --noconfirm $package
				  	
		fi

		# Just checking if installation was successful
		if pacman -Qi $package &> /dev/null; then
		
		echo "################################################################"
		echo "#########  "$package" has installed successfully"
		echo "################################################################"

		else

		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo "!!!!!!!!!  "$package" has NOT been installed"
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

		fi

	fi
		
done


echo "################################################################"
echo "################    extra software installed  ##################"
echo "################################################################"
