#!/bin/bash
set -e

# Overall script with resources.
# Install software and configure each component

# Install File Manager
sh extra-packages/scripts/file-manager.sh

# Extra packages
sh extra-packages/scripts/extra-software.sh
