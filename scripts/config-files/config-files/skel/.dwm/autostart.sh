#!/bin/bash

# Picom
picom -b &

# Enable numlock ( numeric keyboard ), commented out for laptop users
# numlockx &

# Polkit, required for authentication
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
#lxpolkit &

# Keyring for storing saved passwords
gnome-keyring-daemon --start --components=pkcs11 &

# Restore wallpaper
nitrogen --restore &

# Required for xfce settings to work
xfsettingsd &

# Battery power
#xfce4-power-manager &

# Dunst notifications 
dunst -config ~/.config/dunst/dunstrc &

# Side panel shortcuts for file managers
xdg-user-dirs-gtk-update &

# Conky
#conky &
#conky -c ~/.config/conky/arch2.conkyrc & sleep 1

# DWM Blocks
dwmblocks &

# Caffeine
caffeine &
#caffeine --activate &

# Network manager applet
nm-applet &

# Tray volume icons
pasystray --notify=all &

# Tray clipboard manager  
#clipit & 
