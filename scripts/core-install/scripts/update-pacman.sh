#!/bin/bash
set -e

echo "##############	Sync Repos and check for updates	##############"

sudo pacman -Syyy --noconfirm
sudo pacman -Syyu --noconfirm

echo "################################################################"
echo "##############	System updated succesfully	##############"
echo "################################################################"

