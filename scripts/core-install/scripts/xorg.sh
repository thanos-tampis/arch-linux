#!/bin/bash
set -e

echo "##############    Xserver setup and graphics   ##############"

xorgBasePackages='
                xorg-server 
                xorg-apps 
                xorg-xinit 
                xorg-xrandr
                '

for package in $xorgBasePackages
do
 
    sudo pacman -S $package --needed --noconfirm
    
done

# Choose Nvidia or Intel graphics:

# Intel Graphics
function intel_graphics() {
    sudo pacman -S mesa libva-intel-driver --noconfirm --needed
    echo "Intel graphics installed successfully"
    run_graphics_menu
}

# AMD Graphics
function amd_graphics() {
    sudo pacman -S mesa lib32-mesa xf86-video-amdgpu --noconfirm --needed
    echo "AMD graphics installed successfully"
    run_graphics_menu
}

# Nvidia Graphics 
function nvidia_graphics() {

    sudo pacman -S nvidia nvidia-settings --noconfirm --needed
    echo "Nvidia graphics installed successfully"
    run_graphics_menu

}

# VirtualBox Stuff 
function virtualbox_stuff() {
    sudo pacman -S virtualbox-guest-utils linux-headers --noconfirm --needed
    echo "Virtualbox stuff installed successfully"
    run_graphics_menu
}

function run_graphics_menu() {

    COLUMNS=12

    echo "******************************************************************"
    PS3="Enter a number of your choice (1-4):" 

    options=(
        "Intel graphics" 
        "AMD graphics"
        "Nvidia graphics"
        "VirtualBox Stuff ( ONLY if running on Virtualbox )"
    )

    select answer in "${options[@]}" "Quit - No need to install graphics"; do

        case "$REPLY" in
            1)
                intel_graphics                
                ;;
            2)
                amd_graphics                
                ;;
            3)
                nvidia_graphics                
                ;;  
            4)
                virtualbox_stuff                
                ;;                                       
            "Quit - No need to install graphics")
                break
                ;;
            $(( ${#options[@]}+1 )) ) echo "Goodbye!"; exit;;
            *) echo "Invalid option. Try another one.";continue;;
        esac

    done

}

run_graphics_menu


echo "################################################################"
echo "##############    Xserver and graphics installed successfully  ##############"
echo "################################################################"

