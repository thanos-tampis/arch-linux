# Arch installation:

## Notes:

1) On virtualbox usually is dos

- *Verify the boot mode*:

> ls /sys/firmware/efi/efivars

OR

> cat /sys/firmware/efi/fw_platform_size

```
If the command returns 64, then system is booted in UEFI mode and has a 64-bit x64 UEFI. If the command returns 32, 
then system is booted in UEFI mode and has a 32-bit IA32 UEFI; while this is supported, it will limit the boot loader 
choice to systemd-boot. If the file does not exist, the system may be booted in BIOS (or CSM) mode. 
If the system did not boot in the mode you desired (UEFI vs BIOS), refer to your motherboard's manual.
```


- DOS vs UEFI:
	- On partitions:
		- DOS is /boot
		- UEFI is /boot/efi
	- Installing grub packages:
		- DOS only install 'grub, os-prober'
		- UEFI install 'grub, efibootmgr, dosfstools, os-prober, mtools'
	- Installing grub:
		- DOS is 'grub-install --recheck --target=i386-pc /dev/sda'
		- UEFI is 'grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=arch_grub --recheck'

- These are the three differences in the installation process. Notes with added through the guide as well.

- NVME SSDs are usually marked as 'nvme0n1(p-)'. Nothing else is different

2) If you don't wanna have swap partition, you can create a swap file(that will take /root space), have a look after fstab creation section: 


##  ------ PHASE 1 ------

### Connect to Wifi:

1) ip link

2) iwctl

3) *device* list 
 
4) station *device* get-networks 

5) station *device* connect SSID


### Set console keyboard layout:

> localectl list-keymaps
> loadkeys uk

### Update the system clock

> timedatectl set-ntp true


### Create partition stuff:

#### Partition Schema example:

1) /boot or /boot/efi for UEFI

2) swap 
 
3) / 

4) /home 

-------------

Partitions			| Mountpoints	  		| Space 	  | Type 
------------------- | --------------------- |------------ |--------
sda1 -  Master boot |  /boot or /boot/efi   |  	  1gb  	  | primary
sda2 -  swap        |    	  swap  		|     4+gb    | primary
sda3 -  /       	|     	   / 			|    30gb  	  | primary
sda4(5) /home       |		 /home 			|	remaining | extended
 

#### *Note*
For partition, you can either use:

- cfdisk
- fdisk 

```
It is recommended to always use GPT for UEFI boot. 
Use MBR if using Legacy BIOS only
```

1) Using cfdisk:

- cfdisk /dev/sda
- Simple follow screen GUI.

2) Using: fdisk:

- Find your disk label using:
> lsblk

Then start fdisk:
> fdisk /dev/sda

- fdisk notes:
	- Use on-screen help
	- Make boot partition bootable
	- First sectors always leave default(enter)
	- Last sectors example: +10G
	- Don't forget to run 'w' to write changes to disk!


### Format partitions:

1) boot

- If DOS:

	> mkfs.ext4 /dev/sda1

- If UEFI **(Format boot partiion ONLY if Windows are not already installed)**:

	> mkfs.fat -F 32 /dev/sda1

2) root

> mkfs.ext4 /dev/sda3

3) home

> mkfs.ext4 /dev/sda4(5)

4) swap

> mkswap /dev/sda2


### Mount partitions:

1) root

> mount /dev/sda3 /mnt

2) home

> mkdir /mnt/home  
> mount /dev/sda4(5) /mnt/home

3) boot - ONLY on DOS (on UEFI skip this step we'll do that later):

> mkdir /mnt/boot  
> mount /dev/sda1 /mnt/boot

4) swap

> swapon /dev/sda2

5) Create any extra directory and mount that now (e.g Windows directory)


### Update Mirrorlist:

> pacman -S reflector

> reflector --verbose --latest 5 --sort rate --save /etc/pacman.d/mirrorlist 

> pacman -Syy


### Install Arch base:

1) Install Arch:

> pacstrap -K /mnt base linux linux-firmware

2) Generate fstab:

> genfstab -U -p /mnt >> /mnt/etc/fstab

3) Check the resulting /mnt/etc/fstab file, and edit it in case of errors.

> cat /mnt/etc/fstab

### Switch to newly installed Arch system:

> arch-chroot /mnt /bin/bash

#### IF a fileswap need to be created else jump next section:

- fallocate -l 2G /swapfile    
- chmod 600 /swapfile
- mkswap /swapfile
- echo "/swapfile               none                    swap    defaults        0 0" >> /etc/fstab
- swapon /swapfile


### Timezone:

1) Remove current timezone:

> rm -rf /etc/localtime

2) Copy new one:

> ln -s /usr/share/zoneinfo/Europe/London /etc/localtime

3) Set hardware clock:

> hwclock --systohc


### Localization (Language): 

1) Configure Language (uncomment locales):

> nano /etc/locale.gen

- [ en_GB.UTF8 ]

2) Generate locales:

> locale-gen

3) Create the locale.conf file, and set the LANG variable accordingly:

> echo LANG=en_GB.UTF-8 > /etc/locale.conf  
> export LANG=en_GB.UTF-8


### Keyboard layout:

1) List available layouts:
	
> ls /usr/share/kbd/keymaps/\**/*.map.gz

2) Set keyboard layout:

> loadkeys uk

3) Make changes permanent:

> echo KEYMAP=uk > /etc/vconsole.conf


### Setup hostname:

> echo ntenekes > /etc/hostname

### Add matching entries to hosts (/etc/hosts)

> 127.0.0.1		localhost

> ::1			localhost

> 127.0.1.1		*myhostname*.localdomain	*myhostname*

### Network Manager 

1) Install Network Manager

> pacman -S networkmanager

2) Enable Service:

> systemctl enable NetworkManager.service

### Install Extra software

> pacman -S base-devel linux-headers git ntfs-3g

### Create initial ramdisk environment:

> mkinitcpio -P

### Grub:
	
1) If on DOS:

	1) Install grub and os-prober:

		- pacman -S grub os-prober
		- grub-install --target=i386-pc /dev/sda  --recheck

	2) Generate grub.cfg

		- grub-mkconfig -o /boot/grub/grub.cfg

2) If on UEFI:

	1) Create directory and mount:

		- mkdir /boot/efi
		- mount /dev/sda1 /boot/efi  #Mount the FAT32 EFI partition(It should already exists from Windows) 

	2) Install grub, os-prober etc
	
		- pacman -S grub efibootmgr dosfstools os-prober mtools
		- grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB --recheck

	3) Generate grub.cfg

		- grub-mkconfig -o /boot/grub/grub.cfg


### Detecting other operating systems

To detect other OS install on your system, re-run the grub-mkconfig command. If you get the following output: 
Warning: os-prober will not be executed to detect other bootable partitions then edit /etc/default/grub and add/uncomment:

GRUB_DISABLE_OS_PROBER=false

Then try again.


### Change root password:

 > passwd


### Leave chroot environment:

> exit


### Unmount all partition:

> umount -R /mnt


### Reboot:

> reboot


##  ------ PHASE 2 ------


### New user:

1) Add new user:

> useradd -m -G wheel,users -s /bin/bash <user>


### Change user password:

> passwd <user>


### Install sudo:

1) Install sudo:

> pacman -S sudo

2) Edit sudoers:

> EDITOR=nano visudo

- uncomment:

	- %wheel ALL=(ALL) ALL


### Install auto-completion

> pacman -S bash-completion 


### Enable multilib

1) Edit file:

> nano /etc/pacman.conf

- Uncomment:

	- [multilib]
	- Include = /etc/pacman.d/mirrorlist

- Also add (under 'Misc Options'):
	
	- ILoveCandy

2) Update repositories and System:

> pacman -Sy  
> pacman -Syu


### Install AUR Helper

1) Login as user:

> sudo su <user>  
> cd ~
	
2) Install Yay helper:

	- git clone https://aur.archlinux.org/yay.git
	- cd yay
	- makepkg -si
	- cd .. && rm yay


peace