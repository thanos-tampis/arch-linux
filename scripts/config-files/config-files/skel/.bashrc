#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '

# Ensure ~/bin is on the path
if [ -d "$HOME/bin" ] ;
	then PATH="$HOME/bin:$PATH"
fi

# Source shell configs
for f in "$HOME/.bash/"*?.bash; do
    . "$f"
done

# Enable to get Terminal Information on launch
# terminfo

# Enable if no Login Manager is used
# if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
# 	startx
# fi
