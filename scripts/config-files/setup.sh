#!/bin/bash
set -e

# Overall script with resources.
# Configuration

# Copy skel files
sh config-files/scripts/skel-files.sh

# UFW
sh config-files/scripts/ufw.sh

# Misc
sh config-files/scripts/misc.sh

# Default configuration
sh config-files/scripts/default-configuration.sh

# Install DWM, DWMblocks, ST Terminal
sh config-files/scripts/suckless.sh