#!/bin/bash
set -e

# Create directory and Copy custom icons
sudo mkdir -p /usr/share/icons/custom-icons &&
sudo rsync -av config-files/config-files/custom-icons/ /usr/share/icons/custom-icons/ &&
sudo chown -R root:root /usr/share/icons/custom-icons &&
sudo chmod -R 755 /usr/share/icons/custom-icons

# Create directory and Copy wallpapers
mkdir -p ~/Pictures/Wallpapers
rsync -av config-files/config-files/wallpapers/ ~/Pictures/Wallpapers/
chmod -R 755 ~/Pictures/

# Create directory for i3lock-blur
mkdir -p ~/Pictures/i3lock-blur

# Create directory for screenshots
mkdir -p ~/Pictures/screenshots
