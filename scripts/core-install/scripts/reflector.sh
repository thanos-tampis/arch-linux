#!/bin/bash
set -e

echo "##############	Install Reflector	##############"

# installing refector to test wich servers are fastest
sudo pacman -S reflector --noconfirm --needed

# finding the fastest archlinux servers
sudo reflector -l 100 -f 50 --sort rate --threads 5 --verbose --save /tmp/mirrorlist.new && 
rankmirrors -n 0 /tmp/mirrorlist.new > /tmp/mirrorlist && 
sudo cp /tmp/mirrorlist /etc/pacman.d

cat /etc/pacman.d/mirrorlist

echo "##############	Sync Repos and check for updates	##############"

sudo pacman -Syyy --noconfirm
sudo pacman -Syyu --noconfirm

echo "################################################################"
echo "########	Reflector Installed and updates are done	##########"
echo "################################################################"

