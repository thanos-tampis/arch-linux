#!/bin/bash
set -e

# Overall script with resources.
# Configuration

# Generate dynamic menu
# obmenu-generator -p

# Add openbox-session 
# echo -e "\r\n" >> ~/.xinitrc
# echo -e "\r\n# Set Openbox session" >> ~/.xinitrc    
# echo "exec openbox-session" >> ~/.xinitrc 

# Apend aliases
# cat config-files/config-files/bashrc.txt | tee -a ~/.bashrc

# Enable Colors
sudo sed -i "s:#Color:Color:" /etc/pacman.conf

# Copy Dunst files:
#cp -R /usr/share/dunst ~/.config/

# Fix TV desktop app path with current user
CURRENT_USER=$(echo $USER)
sed -i "s/CURRENT_USER/$CURRENT_USER/g" /home/$CURRENT_USER/.local/share/applications/tv.desktop
mkdir /home/$CURRENT_USER/.config/tv
