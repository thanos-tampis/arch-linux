#!/bin/bash

#list
alias l='ls' 					
alias ll='ls -lA'
alias la='ls -A'
alias ls='ls --color=auto'
alias l.="ls -A | egrep '^\.'"      

#fix typo
alias cd..='cd ..'
alias pdw='pwd'
alias vi='nvim'
alias vim='nvim'

# Colorize the grep command output for ease of use
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
	
# Get Fast mirrors
alias mirrors='sudo reflector --score 100 --fastest 25 --sort rate --save /etc/pacman.d/mirrorlist --verbose'	

alias pup='sudo pacman -Syyu' # update
alias pin='sudo pacman -S'    # install
alias pun='sudo pacman -Rs'   # remove
alias pcc='sudo pacman -Scc'  # clear cache
alias pls='pacman -Ql'        # list files
alias prm='sudo pacman -Rnsc' # really remove, configs and all
