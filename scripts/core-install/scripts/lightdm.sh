#!/bin/bash
set -e

echo "##############    Install and enable Lightdm   ##############"

# base lightdm packages
lightdmBasePackages='
				lightdm 
				lightdm-gtk-greeter 
				lightdm-gtk-greeter-settings
				'

for package in $lightdmBasePackages
do
 
	sudo pacman -S $package --needed --noconfirm
	
done


# aur lightdm packages
lightdmAurPackages=' '

for package in $lightdmAurPackages
do 
	
	#checking if application is already installed or else install with aur helpers
	if pacman -Qi $package &> /dev/null; then

		echo "################################################################"
		echo "################## "$package" already installed"
		echo "################################################################"

	else

		#checking which helper is installed
		if pacman -Qi aurman &> /dev/null; then

			echo "Installing with aurman"
			aurman -S --noconfirm --noedit  $package

		elif pacman -Qi trizen &> /dev/null; then
			
			echo "Installing with trizen"
			trizen -S --noconfirm --noedit  $package
			 	
		elif pacman -Qi yay &> /dev/null; then

			echo "Installing with yay"
			yay -S --noconfirm $package
				  	
		fi

		# Just checking if installation was successful
		if pacman -Qi $package &> /dev/null; then
		
		echo "################################################################"
		echo "#########  "$package" has installed successfully"
		echo "################################################################"

		else

		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo "!!!!!!!!!  "$package" has NOT been installed"
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

		fi

	fi
		
done


# enable service
sudo systemctl enable lightdm.service

echo "################################################################"
echo "##############    Lightdm installed successfully  ##############"
echo "################################################################"
