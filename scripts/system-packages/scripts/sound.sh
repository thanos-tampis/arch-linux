#!/bin/bash
set -e

echo "##############    Install sound stuff   ##############"

# base sound packages
soundBasePackages='
				pipewire
				pipewire-alsa
				pipewire-pulse
				pipewire-jack
				helvum
				easyeffects
				pavucontrol
				pamixer
				alsa-utils
				alsa-plugins
				alsa-lib
				alsa-firmware
				gst-plugins-base
				gst-plugins-good
				gst-plugins-bad
				gst-plugins-ugly
				gstreamer
				'

for package in $soundBasePackages
do 
	
	sudo pacman -S $package --needed --noconfirm
	
done


# aur sound packages
soundAurPackages='	
				pasystray
				'

for package in $soundAurPackages
do 
	
	#checking if application is already installed or else install with aur helpers
	if pacman -Qi $package &> /dev/null; then

		echo "################################################################"
		echo "##############    "$package" already installed   ##############"
		echo "################################################################"

	else

		#checking which helper is installed
		if pacman -Qi aurman &> /dev/null; then

			echo "Installing with aurman"
			aurman -S --noconfirm --noedit  $package

		elif pacman -Qi trizen &> /dev/null; then
			
			echo "Installing with trizen"
			trizen -S --noconfirm --noedit  $package
			 	
		elif pacman -Qi yay &> /dev/null; then

			echo "Installing with yay"
			yay -S --noconfirm $package
				  	
		fi

		# Just checking if installation was successful
		if pacman -Qi $package &> /dev/null; then
		
		echo "################################################################"
		echo "#########  "$package" has been installed successfully"
		echo "################################################################"

		else

		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo "!!!!!!!!!  "$package" has NOT been installed"
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

		fi

	fi
	
done


echo "################################################################"
echo "##########   Sound stuff installed successfully   ##############"
echo "################################################################"
