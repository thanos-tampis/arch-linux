#!/bin/bash
set -e

# Overall script with resources.
# Install software and configure each component

# Update Pacman
sh core-install/scripts/update-pacman.sh

# Install reflector
sh core-install/scripts/reflector.sh

# Install xorg stuff
sh core-install/scripts/xorg.sh

# Install Lightdm
sh core-install/scripts/lightdm.sh

# Install Openbox
sh core-install/scripts/install-core.sh
