#!/bin/bash
set -e

# Configuring UFW

# Enable Firewall
sudo ufw enable

sudo ufw default deny
sudo ufw allow from 192.168.0.0/24

# Setup some rules
sudo ufw default allow outgoing
sudo ufw default deny incoming
sudo ufw allow ssh

# Reload Firewall
# sudo ufw reload

# Print Firewall Status
sudo ufw status

# Enable the ufw as a systemd service
sudo systemctl start ufw
sudo systemctl enable ufw
